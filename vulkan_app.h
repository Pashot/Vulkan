#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <memory>
#include <vector>

#include <queue_family.h>

class VulkanApp {
public:
    VulkanApp();
    ~VulkanApp();
    void run();

private:
    /***INIT***/
    void initVulkan();
    void initWindow();

    //INSTANCE
    void createInstance();

    //VALIDATION
    static bool checkValidationLayerSupport();
    std::vector<const char*> getRequiredExtensions();
    //DEBUG MESSENGER
    void setupDebugMessenger();
    VkResult CreateDebugUtilsMessengerEXT(
        VkInstance instance,
        const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDebugUtilsMessengerEXT* pDebugMessenger);
    static void DestroyDebugUtilsMessengerEXT(
        VkInstance instance,
        VkDebugUtilsMessengerEXT debugMessenger,
        const VkAllocationCallbacks* pAllocator);
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData);

    //PHYSICAL DEVICE
    void pickPhysicalDevice();
    bool isDeviceSuitable(VkPhysicalDevice device);
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

    //LOGICAL DEVICE
    void createLogicalDevice();

    //SURFACE
    void createSurface();

    //SWAP CHAIN
    void createSwapChain();
    bool checkDeviceExtensionSupport(VkPhysicalDevice device);
    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

    //IMAGE VIEW
    void createImageViews();

    //PIPELINE
    void createGraphicsPipeline();
    static std::vector<char> readFile(const std::string& filename);
    VkShaderModule createShaderModule(const std::vector<char>& code);
    void createRenderPass();

    //FRAME BUFFER
    void createFramebuffers();

    //COMMAND POOL
    void createCommandPool();
    void createCommandBuffers();

    /***MAIN LOOP***/
    void mainLoop();

    //DRAW FRAME
    void drawFrame();
    void createSemaphores();

    /***CLEAN UP***/
    void cleanup();

private:
    class PrivateData;
    std::unique_ptr<PrivateData> m_pData;
};
