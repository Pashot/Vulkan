#pragma once
#include <optional>
#include <cstdint>
#include <vector>
#include <vulkan/vulkan.h>

class QueueFamilyIndices
{
public:
    std::optional<uint32_t> graphicsFamily;
    std::optional<uint32_t> presentFamily;

public:
    bool isComplete();
};

struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;
};
