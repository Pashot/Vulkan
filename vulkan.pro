TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lglfw -lvulkan -ldl -lpthread -lX11 -lXxf86vm -lXrandr -lXi

SOURCES += \
        main.cpp \
        queue_family.cpp \
        vulkan_app.cpp

HEADERS += \
    queue_family.h \
    vulkan_app.h

RESOURCES += \
        $$PWD/resources.qrc
